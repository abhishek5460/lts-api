import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";

import { UserService } from "src/User/user.service";


@Injectable()
export class TripService{
    constructor(@InjectModel('Trip') private readonly tripModel,
    private readonly userService : UserService){}

    async insertTrip(user_id:string, origin:string,destination:string, cost:number){
        const userfetch = await this.userService.getSingleUser(user_id);
        if(userfetch.user_type==='Client'){
        const newTrip = new this.tripModel({user_id,origin,destination,cost});
        const result = await newTrip.save();
        return user_id;
        }
        return 'Invalid User Type'
    }

    async fetchDrivers(location:string){
        return await this.tripModel.find({origin:location});
    }

    async bookaTrip(tripid:string,driverid:string){
        const fetchedtrip = await this.tripModel.findById(tripid);
        fetchedtrip.status = 'Started';
        fetchedtrip.save();
    }

}