import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { UserModule } from "src/User/user.module";
import { TripController } from "./trip.controller";
import { TripSchema } from "./trip.model";
import { TripService } from "./trip.service";

@Module({
    imports:[MongooseModule.forFeature([{name:'Trip' , schema: TripSchema}]),UserModule],
    controllers:[TripController],
    providers:[TripService]
})

export class TripModule{
}