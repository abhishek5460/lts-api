import { Body,Controller, Get, Post,Param } from "@nestjs/common";
import { TripService } from "./trip.service";

@Controller('trip')
export class TripController{
    constructor(private readonly tripService : TripService){}

    @Post(':id')
    async addTrip(@Param('id') userid:string,
    @Body('origin') origin:string,
    @Body('destination') destination:string,
    @Body('cost') cost:number
    ){
        const generatedTripId = await this.tripService.insertTrip(userid,origin,destination,cost);
        return generatedTripId;
    }

    @Get()
    testingmethod(){
        return "Hello World";
    }

    @Get('/mypath/:location')
    testingmethod1(@Param('location') loc:string){
        return this.tripService.fetchDrivers(loc);
    }

    @Post('/book/:id')
    tripBooked(@Param('id') id:string,
    @Body('driverid') driverid:string){
        return this.tripService.bookaTrip(id,driverid);
    }


}