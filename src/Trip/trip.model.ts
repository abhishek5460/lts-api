import * as mongoose from 'mongoose';

export const TripSchema = new mongoose.Schema({
    // trip_id
    user_id : {type : String},
    origin  : {type : String},
    destination : {type : String},
    cost : {type : Number},
    status : { type:String, enum :['Not Started', 'Started', 'Ended'],default : "Not Started"}
});

export interface Trip {

        user_id : string, 
        origin:string, 
        destination : string, 
        cost:number,
        status : string
}