import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
    user_name : {type : String, required : true},
    user_type  : {type : String,enum:['Client', 'Driver']},
    user_contact : {type : Number, required : true},
    user_city : {type : String, required : true}
});


export interface User {
        user_name:string, 
        user_type : string, 
        user_contact:number,
        user_city:string
}