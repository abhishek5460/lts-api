import { Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";
import { UserService } from "./user.service";

@Controller('user')
export class UserController{
    constructor(private readonly userService : UserService){}
    //Endpoint for adding user
    @Post() 
    async addUser(@Body('user_name') username:string, 
    @Body('user_type') usertype:string , 
    @Body('user_contact') usercontact:number, 
    @Body('user_city') usercity:string 
    ){
        const generatedId = await this.userService.insertUser(username,usertype,usercontact,usercity);
        return {id : generatedId};
    }

    //Endpoint for getting all users
    @Get()
    async getAllUsers(){

        const Users = await this.userService.getAllUsers();
        return Users.map(user =>({
            id : user.id,
            user_name : user.user_name,
            user_type : user.user_type,
            user_city : user.user_city,
            user_contact : user.user_contact,
        }));
    }

    //endpoint for getting user by id
    @Get(':id')
    getSingleUser(@Param('id') userId:string){
        return this.userService.getSingleUser(userId);
    }

    //updating user
    @Patch(':id')
    updateUser(@Param('id') userid:string,
    @Body('user_name') username:string,
    @Body('user_type') usertype:string ,
    @Body('user_city') usercity:string,
    @Body('user_contact') usercontact:number){
        this.userService.updateUser(userid,username,usertype,usercity,usercontact);
        return null;
    }

    //deleting user
    @Delete(':id')
    async deleteUser(@Param('id')userid:string){
        await this.userService.deleteUser(userid);
        return null;
    }


}