import { Injectable, NotFoundException, UnsupportedMediaTypeException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";

import {User} from './user.model';

@Injectable()
export class UserService{

    constructor(@InjectModel('User') private readonly userModel : Model<User>){}

    async insertUser(user_name:string,user_type:string, user_contact:number,user_city:string){
        const newUser = new this.userModel({user_name,user_type,user_contact,user_city});
        const result  = await newUser.save();
        return result.id as string;
    }

    async getAllUsers(){
        const users = await this.userModel.find().exec();
        return users ;
    }

    async getSingleUser(userId:string){
        const user = await this.findUser(userId);
        return user;
    }

    async updateUser(userId:string,user_name:string, user_type:string, user_city:string, user_contact:number){
        const updatedUser = await this.userModel.findById(userId);
        if(user_name){
            updatedUser.user_name = user_name;
        }
        if(user_type){
            try {
                updatedUser.user_type = user_type ;
            }
            catch {
                throw new UnsupportedMediaTypeException;
            }
        }
        if(user_city){
            updatedUser.user_city = user_city;
        }
        if(user_contact){
            updatedUser.user_contact = user_contact;
        }
        updatedUser.save();
    }

    async deleteUser(userId:string){
        const result = await this.userModel.deleteOne({_id:userId}).exec();
        if(result.deletedCount===0){
            throw new NotFoundException('Could not find the product');
        }
    }

    private async findUser(userId:string){
        const user  = await this.userModel.findById(userId);
        if(!user) {
            throw new NotFoundException('Could not find the user');
        }
        return {
            id : user.id,
            user_name : user.user_name,
            user_type : user.user_type,
            user_city : user.user_city,
            user_contact : user.user_contact
        }
    }


}