import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'


import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './User/user.module';
import { TripModule } from './Trip/trip.module';

@Module({
  imports: [UserModule,TripModule ,MongooseModule.forRoot('mongodb+srv://dbuser:Zoi9iQ4Yc0U8HkuL@cluster0.dwth7.mongodb.net/ltsDb?retryWrites=true&w=majority')],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
